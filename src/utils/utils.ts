import {TUtils} from "./types/utilsTypes"


export class Utils {
    static CreateCustomElement<T extends HTMLElement, C extends HTMLElement | string>({tagName, classNameOne , classNameTwo, content, id}: TUtils<C>): T {
        const tag = document.createElement(tagName) as T
        tag.classList.add(classNameOne, classNameTwo)
        tag.append(content)
        tag.id = id

        return tag
    }
}
