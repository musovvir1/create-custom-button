export type TUtils<C> = {
  tagName: string;
  classNameOne: string;
  classNameTwo: string;
  content: C;
  id: string;
};
