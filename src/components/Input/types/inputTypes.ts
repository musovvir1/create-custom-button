export type TInput = {
  divInput: HTMLDivElement;
  inpType: string;
  inpPlaceholder: string;
  regularEmail: RegExp;
  iconExit: string;
};
