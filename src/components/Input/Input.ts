import { Utils } from "../../utils/utils";
import { TInput } from "./types/inputTypes";

export const Input = ({
  divInput,
  inpType,
  inpPlaceholder,
  regularEmail,
  iconExit,
}: TInput): HTMLInputElement => {
  const inputVal: HTMLInputElement = Utils.CreateCustomElement<
    HTMLInputElement,
    string
  >({
    tagName: "input",
    classNameOne: "inputText",
    classNameTwo: "null",
    content: "",
    id: "email",
  });

  const removeValidation: HTMLElement = Utils.CreateCustomElement<
    HTMLElement,
    string
  >({
    tagName: "i",
    classNameOne: "fas",
    classNameTwo: iconExit,
    content: "",
    id: "",
  });

  inputVal.placeholder = inpPlaceholder;
  inputVal.type = inpType;

  inputVal.addEventListener("focus", (e) => {
    if (!(e.target as HTMLInputElement).value) {
      divInput.innerText = "Пожалуйста, заполните это поле";
      divInput.classList.add("error");
      divInput.classList.remove("toggleVal");
      divInput.append(removeValidation);
    } else if (!regularEmail.test(inputVal.value)) {
      divInput.innerText = "Пожалуйста, введите правильный email";
      divInput.classList.add("error");
      divInput.classList.remove("toggleVal");
      divInput.append(removeValidation);
    } else if ((e.target as HTMLInputElement).value.length > 200) {
      divInput.innerText = "Максимальное количество символов";
      divInput.classList.add("error");
      divInput.append(removeValidation);
    } else if (!inputVal.validity.typeMismatch) {
      divInput.innerText = "";
      divInput.classList.remove("error");
    }
  });

  inputVal.addEventListener("blur", (e) => {
    if (!(e.target as HTMLInputElement).value) {
      divInput.innerText = "Пожалуйста, заполните это поле";
      divInput.append(removeValidation);
      divInput.classList.remove("toggleVal");
    } else if (!regularEmail.test(inputVal.value)) {
      divInput.innerText = "Пожалуйста, введите правильный email";
      divInput.classList.remove("toggleVal");
      divInput.classList.add("error");
      divInput.append(removeValidation);
    } else if ((e.target as HTMLInputElement).value.length > 200) {
      divInput.innerText = "Максимальное количество символов";
      divInput.append(removeValidation);
      divInput.classList.add("error");
    } else if (!inputVal.validity.typeMismatch) {
      divInput.innerText = "";
      divInput.classList.remove("error");
    }
  });

  removeValidation.addEventListener("click", () => {
    divInput.classList.add("toggleVal");
  });

  return inputVal;
};
