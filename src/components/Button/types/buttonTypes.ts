export type TButton = {
  name: string;
  type: string;
  toggleSpin: () => void;
  width: string;
  load: boolean;
  iconLeft: string;
  iconRight: string;
};
