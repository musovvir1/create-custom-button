import { Utils } from '../../utils/utils';
import { TButton } from './types/buttonTypes';

export const Button = ({
  name,
  type,
  toggleSpin,
  width,
  load,
  iconLeft,
  iconRight,
}: TButton): HTMLButtonElement => {
  const button: HTMLButtonElement = Utils.CreateCustomElement<
    HTMLButtonElement,
    string
  >({
    tagName: 'button',
    classNameOne: type,
    classNameTwo: width,
    content: '',
    id: '',
  });
  const text: HTMLSpanElement = Utils.CreateCustomElement<
    HTMLSpanElement,
    string
  >({
    tagName: 'span',
    classNameOne: 'null',
    classNameTwo: 'null',
    content: name,
    id: '',
  });

  if (iconLeft) {
    const iconArrowLeft: HTMLElement = Utils.CreateCustomElement<
      HTMLElement,
      string
    >({
      tagName: 'i',
      classNameOne: 'fas',
      classNameTwo: iconLeft,
      content: '',
      id: '',
    });
    text.prepend(iconArrowLeft);
  }

  if (iconRight) {
    const iconArrowRight: HTMLElement = Utils.CreateCustomElement<
      HTMLElement,
      string
    >({
      tagName: 'i',
      classNameOne: 'fas',
      classNameTwo: iconRight,
      content: '',
      id: '',
    });

    text.append(iconArrowRight);
  }
  
  if (load) {
    const image: HTMLImageElement = Utils.CreateCustomElement<
      HTMLImageElement,
      string
    >({
      tagName: 'img',
      classNameOne: 'spin',
      classNameTwo: 'null',
      content: '',
      id: '',
    });
    image.src = 'https://i.stack.imgur.com/hu3Fv.png';
    button.append(image);
  } else {
    button.append(text);
  }

  button.addEventListener('click', toggleSpin);

  return button;
};
