import { Input } from "./components/Input/Input";
import { Utils } from "./utils/utils";
import { Button } from "./components/Button/Button";

export const App = (): HTMLDivElement | Node => {
  const app: HTMLDivElement = document.querySelector("#App");

  const btnWrapper: HTMLDivElement = Utils.CreateCustomElement<
    HTMLDivElement,
    string
  >({
    tagName: "div",
    classNameOne: "null",
    classNameTwo: "null",
    content: "",
    id: "",
  });

  const divArbitrary: HTMLDivElement = Utils.CreateCustomElement<
    HTMLDivElement,
    string
  >({
    tagName: "div",
    classNameOne: "divArb",
    classNameTwo: "null",
    content: "",
    id: "",
  });

  const divInput: HTMLDivElement = Utils.CreateCustomElement<
    HTMLDivElement,
    string
  >({
    tagName: "div",
    classNameOne: "null",
    classNameTwo: "null",
    content: "",
    id: "errorDiv",
  });

  const regularEmail: RegExp =
    /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{0,201}$/;

  let load = false;

  const toggleSpin = () => {
    load = !load;

    render();
  };

  const render = () => {
    inputValidate();

    btnWrapper.innerHTML = "";

    btnWrapper.append(
      Input({
        divInput,
        inpType: "text",
        inpPlaceholder: "Введите email",
        regularEmail,
        iconExit: "fa-times",
      })
    );

    btnWrapper.append(divInput);

    btnWrapper.append(
      Button({
        name: "Кнопка",
        type: "btn",
        toggleSpin,
        width: "buttonWidth",
        load,
        iconLeft: "fa-arrow-right",
        iconRight: "fa-arrow-left",
      })
    );

    btnWrapper.append(divArbitrary);
  };

  const inputValidate = () => {
    const inp = document.querySelector("#email") as HTMLInputElement;
    const divInputValue: HTMLDivElement = Utils.CreateCustomElement<
      HTMLDivElement,
      string
    >({
      tagName: "div",
      classNameOne: "divInp",
      classNameTwo: "null",
      content: "",
      id: "",
    });
    divInputValue.innerText += inp.value.trim();
    divArbitrary.append(divInputValue);
  };

  app.append(btnWrapper);

  btnWrapper.append(
    Input({
      divInput,
      inpType: "text",
      inpPlaceholder: "Введите email",
      regularEmail,
      iconExit: "fa-times",
    })
  );

  btnWrapper.append(divInput);

  btnWrapper.append(
    Button({
      name: "Кнопка",
      type: "btn",
      toggleSpin,
      width: "buttonWidth",
      load,
      iconLeft: "fa-arrow-right",
      iconRight: "fa-arrow-left",
    })
  );

  btnWrapper.append(divArbitrary);

  return app;
};
